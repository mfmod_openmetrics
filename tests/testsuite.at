## Makefile for the mfmod_openmetrics testsuite.
## Process this file with automake to produce Makefile.in.
## Copyright (C) 2022-2023 Sergey Poznyakoff
##
## Mfmod_openmetrics is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by the
## Free Software Foundation; either version 3 of the License, or (at your
## option) any later version.
##
## Mfmod_openmetrics is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with mfmod_openmetrics. If not, see <http://www.gnu.org/licenses/>. */
##
m4_version_prereq([2.69])
m4_define([MAILFROMD_PREREQ],[if test "$MAILFROMD" = "false"; then
  exit 77
fi])

m4_define([MFT_RUN],
[AT_CHECK([MAILFROMD_PREREQ
cat > mfmod.conf <<EOT
listen any;
$1
EOT
AT_DATA([prog],[$2])
TMP=$(pwd)
MFMOD_OPENMETRICS_CONF=$(pwd)/mfmod.conf
export TMP MFMOD_OPENMETRICS_CONF
$MAILFROMD --run prog $3
],
m4_shift3($@))
])

AT_INIT
m4_include([eof.at])
m4_include([declare.at])
m4_include([incr01.at])
m4_include([incr02.at])
m4_include([incr03.at])
m4_include([add.at])
m4_include([set.at])
m4_include([duration.at])

m4_include([namespace.at])
m4_include([persistent.at])
