/* This file is part of mfmod_openmetrics.
 * Copyright (C) 2022-2023 Sergey Poznyakoff
 *
 * Mfmod_openmetrics is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * Mfmod_openmetrics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mfmod_openmetrics.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "config.h"
#include <mailfromd/mfmod.h>
#include <mailfromd/exceptions.h>
#include <mailutils/mailutils.h>
#include <gdbm.h>
#include <inttypes.h>
#include <microhttpd.h>
#include <ctype.h>
#include <sys/socket.h>
#include <netdb.h>
#include <json.h>

static int
egdbm(MFMOD_PARAM *r)
{
	return mfmod_error(r, mfe_dbfailure, "%s", gdbm_strerror(gdbm_errno));
}


typedef enum {
	value_counter,
	value_gauge,
	value_duration
} METRIC_TYPE;

typedef struct {
	datum name;
	int type;
	char *family_name;
	char const *unit;
	struct json_value *json;
} METRIC;

static char const *
metric_type_str(METRIC_TYPE t)
{
	static char *types[] = {
		[value_counter]  = "counter",
		[value_gauge]    = "gauge",
		[value_duration] = "gauge"
	};
	if (t < 0 || t >= sizeof(types)/sizeof(types[0]))
		return NULL;
	return types[t];
}

#define TYPE_ATTR "type"
#define HELP_ATTR "help"
#define UNIT_ATTR "unit"
#define FLAGS_ATTR "flags"
#define VALUES_ATTR "values"


#define DEFAULT_NODE "127.0.0.1"
#define DEFAULT_SERVICE "8080"
#define DEFAULT_TRUSTED_IP "127.0.0.0/8"
#define DEFAULT_DBNAME "mfmetrics.db"

#define DEFAULT_LISTEN_ADDRESS DEFAULT_NODE ":" DEFAULT_SERVICE
#define TEMPLATE DEFAULT_DBNAME ".XXXXXX"

static int persistent;
static char *dbname;
static struct MHD_Daemon *mhd;

static char *listen_address;
static int httpd_access_log;
static mu_list_t trusted_ip_list;
static char *namespace;
static unsigned db_max_retry = 10;
static struct timespec db_sleep = { 0, 100*1e6 };
static char *config_file = SYSCONFDIR "/mfmod_openmetrics.conf";

static int
cidr_comp(const void *a, const void *b)
{
	struct mu_cidr *ca = (struct mu_cidr *)a;
	struct mu_cidr *cb = (struct mu_cidr *)b;
	return mu_cidr_match(ca, cb);
}

static void
fill_trusted_ip_list(char *val)
{
       char *p;
       for (p = strtok(val, ","); p; p = strtok(NULL, ",")) {
	       struct mu_cidr cidr;
	       if (mu_cidr_from_string(&cidr, p) == 0) {
		       struct mu_cidr *cp = mu_alloc(sizeof(*cp));
		       memcpy(cp, &cidr, sizeof(*cp));
		       mu_list_append(trusted_ip_list, cp);
	       }
       }
}

/* Configuration */
static int
cidr_parser(void *item, void *data)
{
	struct mu_cidr cidr, *cp;
	mu_config_value_t *val = item;

	if (mu_cfg_assert_value_type(val, MU_CFG_STRING))
		return 1;
	if (mu_cidr_from_string(&cidr, val->v.string)) {
		mu_error("not a CIDR: %s", val->v.string);
		return 1;
	} else {
		cp = mu_alloc(sizeof(*cp));
		memcpy(cp, &cidr, sizeof(*cp));
		mu_list_append(trusted_ip_list, cp);
	}
	return 0;
}

static int
cb_trusted_ip(void *data, mu_config_value_t *val)
{
	struct mu_cidr cidr, *cp;
	int i;

	if (trusted_ip_list) {
		mu_list_create(&trusted_ip_list);
		mu_list_set_comparator(trusted_ip_list, cidr_comp);
	}

	switch (val->type) {
	case MU_CFG_STRING:
		if (mu_cidr_from_string(&cidr, val->v.string)) {
			mu_error("not a CIDR: %s", val->v.string);
			return 1;
		} else {
			cp = mu_alloc(sizeof(*cp));
			memcpy(cp, &cidr, sizeof(*cp));
			mu_list_append(trusted_ip_list, cp);
		}
		break;

	case MU_CFG_LIST:
		mu_list_foreach(val->v.list, cidr_parser, NULL);
		break;

	case MU_CFG_ARRAY:
		for (i = 0; i < val->v.arg.c; i++) {
			cidr_parser(&val->v.arg.v[i], NULL);
		}
		break;
	}
	return 0;
}

static int
cb_database_wait(void *data, mu_config_value_t *val)
{
	long n;
	char *p;

	if (mu_cfg_assert_value_type(val, MU_CFG_STRING))
		return 1;
	if (val->v.string[0] == '.') {
		n = 0;
		p = (char*) val->v.string;
	} else {
		errno = 0;
		n = strtol(val->v.string, &p, 10);
		if (errno || n < 0) {
			mu_error("timeout must be a non-negative floating point number");
			return 1;
		}
	}
	db_sleep.tv_sec = n;
	if (*p == 0)
		db_sleep.tv_nsec = 0;
	else if (*p == '.') {
		int i;
		char *q;

		p++;
		n = strtol(p, &q, 10);
		if (errno || *q || n < 0) {
			mu_error("timeout must be a non-negative floating point number");
			return 1;
		}
		if (n > 1e9)
			db_sleep.tv_nsec = n / 1e9;
		else
			for (i = q - p; i < 9; i++)
				n *= 10;
		db_sleep.tv_nsec = n;
	}

	return 0;
}

static struct mu_cfg_param openmetrics_param[] = {
	{
		.ident = "listen",
		.type = mu_c_string,
		.data = &listen_address,
		.docstring = "Listen on this address.",
		.argname = "addr: [IP:]PORT"
	},
	{
		.ident = "access-log",
		.type = mu_c_bool,
		.data = &httpd_access_log,
		.docstring = "Enable HTTP access log.",
	},
	{
		.ident = "trusted-ip",
		.type = mu_cfg_callback,
		.callback = cb_trusted_ip,
		.docstring = "Trusted IP addresses."
	},
	{
		.ident = "namespace",
		.type = mu_c_string,
		.docstring = "Namespace prefix for metric names.",
		.data = &namespace
	},
	{
		.ident = "persistent",
		.type = mu_c_bool,
		.docstring = "Persistent database.",
		.data = &persistent
	},
	{
		.ident = "database-name",
		.type = mu_c_string,
		.docstring = "Database name to use.",
		.data = &dbname
	},
	{
		.ident = "database-retry",
		.type = mu_c_uint,
		.docstring = "Number of times to retry opening database"
		             " when it is in use by another process.",
		.data = &db_max_retry
	},
	{
		.ident = "database-wait",
		.type = mu_cfg_callback,
		.callback = cb_database_wait,
		.docstring = "Wait that many seconds for the database to be unlocked"
		             " when it is in use by another process.",
		.argname = "SEC.FRAC"
	},
	{ NULL }
};

static char *
fulldbname(char const *name)
{
	char *tmpdir = getenv("TMP");
	if (!tmpdir)
		tmpdir = "/tmp";

	return mu_make_file_name(tmpdir, name);
}

static int
conf_read(char const *filename)
{
	mu_cfg_tree_t *tree;

	if (mu_cfg_parse_file(&tree, filename, 0))
		return -1;
	mu_cfg_tree_reduce(tree, NULL, openmetrics_param, NULL);
	mu_cfg_destroy_tree(&tree);

	if (persistent) {
		if (!dbname)
			dbname = fulldbname(DEFAULT_DBNAME);
		else {
			mu_normalize_path(dbname);
			if (dbname[0] != '/') {
				char *p;
				p = fulldbname(dbname);
				free(dbname);
				dbname = p;
			}
		}
	}
	return 0;
}

static int
openmetrics_dbopen(GDBM_FILE *ret_dbf, MFMOD_PARAM *r)
{
	GDBM_FILE dbf;
	if (!dbname) {
		int fd;
		char *namebuf = fulldbname(TEMPLATE);

		if ((fd = mkstemp(namebuf)) == -1) {
			free(namebuf);
			r->type = mfmod_string;
			r->string = strerror(errno);
			return mfe_failure;
		}

		dbf = gdbm_fd_open(fd, namebuf, 0, GDBM_NEWDB, NULL);

		if (dbf == NULL) {
			free(namebuf);
			close(fd);
			return egdbm(r);
		}
		dbname = namebuf;
	} else {
		int i;

		for (i = 0; i < db_max_retry; i++) {
			dbf = gdbm_open(dbname, 0, GDBM_WRCREAT, 0640, NULL);
			if (dbf != NULL)
				goto success;
			if (gdbm_errno != GDBM_CANT_BE_WRITER)
				return egdbm(r);
			nanosleep(&db_sleep, NULL);
		}
		return egdbm(r);
	}

success:
	*ret_dbf = dbf;

	return 0;
}

static void
cleanup(void)
{
	if (!persistent && dbname) {
		unlink(dbname);
		free(dbname);
		dbname = NULL;
	}
}

static int server_start(MFMOD_PARAM *r);


/*
 * openmetrics_init()
 * ------------------
 * Initializes the openmetrics module: reads the configuration file,
 * tries to open the database, creating it if it doesn't exist and
 * starts the HTTP server thread.
 *
 * Arguments: none
 * Return: none
 * Exceptions:
 *  mfe_failure    Failed to create temporary database file.
 *  mfe_inval      Bad number of arguments.
 *  mfe_dbfailure  GDBM error.
 */
int
openmetrics_init(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	int rc;
	GDBM_FILE dbf;
	char *s;

	ASSERT_ARGCOUNT(r, count, 0);

	if ((s = getenv("MFMOD_OPENMETRICS_CONF")) != NULL) {
		if (conf_read(s))
			return mfmod_error(r, mfe_inval,
					   "errors parsing configuration file %s",
					   s);
	} else if (access(config_file, F_OK) == 0) {
		conf_read(config_file);
	}

	if ((rc = openmetrics_dbopen(&dbf, r)) != 0)
		return rc;
	gdbm_close(dbf);

	r->type = mfmod_number;
	r->number = 0;
	return server_start(r);
}

/*
 * openmetrics_destroy()
 * ---------------------
 * Shuts down the openmetrics module and releases the resources allocated
 * for it.  The database file is deleted, unless 'persistent on' is set
 * in configuration.
 *
 * Arguments: none
 * Return: none
 */
int
openmetrics_destroy(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	ASSERT_ARGCOUNT(r, count, 0);
	MHD_stop_daemon(mhd);
	cleanup();
	r->type = mfmod_number;
	r->number = 0;
	return 0;
}

/*
 * openmetrics_http_address()
 * -------------------------
 * Returns the network address the module listens on for the incoming HTTP
 * requests.
 *
 * Arguments: none
 * Returns: string in the form "IP:PORT".
 */
int
openmetrics_http_address(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	ASSERT_ARGCOUNT(r, count, 0);
	r->type = mfmod_string;
	r->string = mu_strdup(listen_address ? listen_address : DEFAULT_LISTEN_ADDRESS);
	return 0;
}

static void
write_pool(void *data, char const *str, size_t len)
{
	mu_opool_t pool = data;

	mu_opool_append(pool, str, len);
}

static void
metric_set_type(METRIC *mtr, int type)
{
	struct json_value *jv = json_new_integer(type);
	json_object_set(mtr->json, TYPE_ATTR, jv);
	mtr->type = type;
}

static void
metric_set_family_name(METRIC *mtr)
{
	char *family_name, *p;
	size_t namelen = mtr->name.dsize;

	if (namespace)
		namelen += strlen(namespace) + 1;
	if (mtr->unit)
		namelen += strlen(mtr->unit) + 1;

	family_name = mu_alloc(namelen + 1);

	p = family_name;

	if (namespace) {
		strcpy(p, namespace);
		p += strlen(namespace);
		*p++ = '_';
	}

	memcpy(p, mtr->name.dptr, mtr->name.dsize);
	p += mtr->name.dsize;

	if (mtr->unit) {
		*p++ = '_';
		strcpy(p, mtr->unit);
		p += strlen(mtr->unit);
	}
	*p = 0;

	free(mtr->family_name);
	mtr->family_name = family_name;
}

static void
metric_set_unit(METRIC *mtr, char const *unit)
{
	if (unit) {
		struct json_value *jv = json_new_string(unit);
		json_object_set(mtr->json, UNIT_ATTR, jv);
		mtr->unit = jv->v.s;
	} else {
		json_object_set(mtr->json, UNIT_ATTR, json_new_null());
		mtr->unit = NULL;
	}
	metric_set_family_name(mtr);
}

static void
metric_set_help(METRIC *mtr, char const *help)
{
	json_object_set(mtr->json, HELP_ATTR, json_new_string(help));
}

static char const *
metric_get_help(METRIC *mtr)
{
	struct json_value *jv;
	if (json_object_get(mtr->json, HELP_ATTR, &jv) == 0 && jv->type == json_string)
		return jv->v.s;
	return NULL;
}

static char const *
metric_get_unit(METRIC *mtr)
{
	return mtr->unit;
}

static int
metric_create(METRIC **pmtr, datum *name, int type)
{
	METRIC *mtr = mu_calloc(1, sizeof(*mtr));

	mtr->name.dsize = name->dsize;
	mtr->name.dptr = mu_alloc(name->dsize);
	memcpy(mtr->name.dptr, name->dptr, name->dsize);
	mtr->json = json_new_object();
	metric_set_type(mtr, type);
	metric_set_unit(mtr, type == value_duration ? "seconds" : NULL);
	json_object_set(mtr->json, VALUES_ATTR, json_new_object());

	*pmtr = mtr;

	return 0;
}

static int
metric_encode(datum *dp, METRIC *mtr)
{
	struct json_format format = {
		.indent = 0,
		.precision = 6,
		.write = write_pool
	};
	mu_opool_t pool;
	int rc;

	mu_opool_create(&pool, MU_OPOOL_ENOMEMABRT);
	format.data = pool;
	rc = json_value_format(mtr->json, &format, 0);
	if (rc == 0) {
		size_t size;
		dp->dptr = mu_opool_detach(pool, &size);
		dp->dsize = size;
	}
	mu_opool_destroy(&pool);
	return rc;
}

static int
metric_decode(datum *dp, datum *key, METRIC **ret_metric, MFMOD_PARAM *retval)
{
	int rc;
	char *errp;
	struct json_value *json, *jv;
	int type;

	if ((rc = json_parse_string(dp->dptr, &json, &errp)) != JSON_E_NOERR) {
		return mfmod_error(retval, mfe_dbfailure,
				   "error parsing content for key %*.*s: %s near %s",
				   key->dsize, key->dsize, key->dptr,
				   json_strerror(rc),
				   *errp == 0 ? "end" : errp);
	}

	if (json->type != json_object) {
		rc = mfmod_error(retval, mfe_dbfailure,
				 "bad content type for key %*.*s",
				 key->dsize, key->dsize, key->dptr);
	} else if (json_object_get(json, TYPE_ATTR, &jv)) {
		rc = mfmod_error(retval, mfe_dbfailure,
				 "%*.*s: no type attribute",
				 key->dsize, key->dsize, key->dptr);
	} else if (jv->type == json_number) {
		jv->type = json_integer;
		type = (int) jv->v.n;
		switch (type) {
		case value_counter:
		case value_gauge:
		case value_duration:
			break;

		default:
			rc = mfmod_error(retval, mfe_dbfailure,
					 "%*.*s: bad type (%d)",
					 key->dsize, key->dsize, key->dptr, type);
		}
	} else {
		rc = mfmod_error(retval, mfe_dbfailure,
				 "%*.*s: bad type attribute",
				 key->dsize, key->dsize, key->dptr);
	}

	if (rc) {
		json_value_free(json);
	} else {
		METRIC *mtr = mu_calloc(1, sizeof(*mtr));

		mtr->name.dsize = key->dsize;
		mtr->name.dptr = mu_alloc(key->dsize);
		memcpy(mtr->name.dptr, key->dptr, key->dsize);
		mtr->json = json;
		mtr->type = type;

		if (json_object_get(mtr->json, UNIT_ATTR, &jv) == 0 &&
		    jv->type == json_string)
			mtr->unit = jv->v.s;

		metric_set_family_name(mtr);

		/* Fixup flags */
		if (json_object_get(mtr->json, FLAGS_ATTR, &jv) == 0) {
			if (jv->type == json_number)
				jv->type = json_integer;
			else {
				jv = json_new_integer(0);
				json_object_set(mtr->json, FLAGS_ATTR, jv);
			}
		}

		*ret_metric = mtr;
	}

	return rc;
}

static void
metric_free(METRIC *mtr)
{
	if (mtr) {
		free(mtr->name.dptr);
		free(mtr->family_name);
		json_value_free(mtr->json);
		free(mtr);
	}
}

static void
metric_destroy(METRIC **pmtr)
{
	if (pmtr) {
		metric_free(*pmtr);
		*pmtr = NULL;
	}
}

static int update_reset(int type, struct json_value *jv, void *data, MFMOD_PARAM *retval);

static int
dbmodify(datum key, char const *labelset,
	 int (*update)(int, struct json_value *, void *, MFMOD_PARAM *), void *data,
	 int ifnew,
	 MFMOD_PARAM *retval)
{
	GDBM_FILE dbf;
	datum content;
	int rc;
	METRIC *mtr;
	char *p;

	if ((rc = openmetrics_dbopen(&dbf, retval)) != 0)
		return rc;

	content = gdbm_fetch(dbf, key);
	if (content.dptr == NULL) {
		if (gdbm_errno == GDBM_ITEM_NOT_FOUND) {
			return mfmod_error(retval, mfe_not_found,
					   "metric %*.*s not declared",
					   key.dsize, key.dsize, key.dptr);
		} else {
			return egdbm(retval);
		}
	}

	if ((p = realloc(content.dptr, content.dsize + 1)) != NULL) {
		content.dptr = p;
		content.dptr[content.dsize] = 0;
		rc = metric_decode(&content, &key, &mtr, retval);
	} else {
		rc = -1;
	}
	free(content.dptr);
	if (rc == 0) {
		struct json_value *val, *jv;

		if (json_object_get(mtr->json, VALUES_ATTR, &val)) {
			val = json_new_object();
			json_object_set(mtr->json, VALUES_ATTR, val);
		}

		if (json_object_get(val, labelset, &jv)) {
			jv = json_new_number(0);
			update_reset(mtr->type, jv, NULL, retval);
			json_object_set(val, labelset, jv);
		} else if (ifnew)
			goto end;
		if ((rc = update(mtr->type, jv, data, retval)) == 0) {
			if (metric_encode(&content, mtr)) {
				rc = mfmod_error(retval, mfe_failure, "%s",
						 "can't encode content");
			} else {
				if (gdbm_store(dbf, key, content, GDBM_REPLACE))
					rc = egdbm(retval);
				free(content.dptr);
			}
		}
	end:
		metric_free(mtr);
	}
	gdbm_close(dbf);
	if (rc == 0) {
		retval->type = mfmod_number;
		retval->number = 0;
	}
	return rc;
}

/*
 * openmetrics_declare(name, type, help, ...)
 * ------------------------------------------
 * Declares the metric.
 *
 * Arguments:
 *  p[0]   string      Metric name
 *  p[1]   number      Metric type
 *  p[2]   string      Help text
 *  p[3]   string      LabelSet of the first metric to initialize.
 *  ...
 *  p[count-1] string  LabelSet of the first metric to initialize.
 *
 * Return: none
 *
 * Exceptions:
 *  mfe_failure    Failed to create temporary database file.
 *  mfe_inval      Bad number of arguments, or argument types, or bad metric
 *                 type given.
 *  mfe_dbfailure  GDBM error.
 */
int
openmetrics_declare(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	GDBM_FILE dbf;
	datum key, content;
	int type;
	METRIC *mtr;
	struct json_value *jv;
	int rc = 0;
	int modified = 0;
	long i;

	if (count < 3) {
		return mfmod_error(r, mfe_inval, "%s",
				   "bad number of arguments");
	}
	ASSERT_ARGTYPE(p, r, 0, mfmod_string);
	ASSERT_ARGTYPE(p, r, 1, mfmod_number);
	ASSERT_ARGTYPE(p, r, 2, mfmod_string);
	for (i = 3; i < count; i++)
		ASSERT_ARGTYPE(p, r, i, mfmod_string);

	switch (type = p[1].number) {
	case value_counter:
	case value_gauge:
	case value_duration:
		break;
	default:
		r->type = mfmod_string;
		r->string = mu_strdup("invalid data type");
		return mfe_inval;
	}

	if ((rc = openmetrics_dbopen(&dbf, r)) != 0)
		return rc;

	key.dptr = p->string;
	key.dsize = strlen(key.dptr);

	content = gdbm_fetch(dbf, key);
	if (content.dptr == NULL) {
		struct json_value *val;

		metric_create(&mtr, &key, type);
		metric_set_help(mtr, p[2].string);
		json_object_get(mtr->json, VALUES_ATTR, &val);
		for (i = 3; i < count; i++) {
			jv = json_new_number(0);
			json_object_set(val, p[i].string, jv);
			if ((rc = update_reset(type, jv, NULL, r)) != 0)
				goto err;
		}
		modified = 1;
	} else {
		if ((rc = metric_decode(&content, &key, &mtr, r)) != 0)
			goto err;

		if (mtr->type != type) {
			rc = mfmod_error(r, mfe_dbfailure,
					 "metric %s is already declared to be of type %d", p->string, type);
			goto err;
		}

		if (json_object_get(mtr->json, HELP_ATTR, &jv) ||
		    jv->type != json_string ||
		    strcmp(jv->v.s, p[2].string)) {
			metric_set_help(mtr, p[2].string);
			modified = 1;
		}
	}
err:
	free(content.dptr);
	if (modified) {
		if (metric_encode(&content, mtr)) {
			rc = mfmod_error(r, mfe_failure, "%s",
					   "can't encode content");
		}

		if (gdbm_store(dbf, key, content, GDBM_REPLACE))
			rc = egdbm(r);

		free(content.dptr);
	}
	metric_free(mtr);
	gdbm_close(dbf);
	if (rc)
		return rc;

	r->type = mfmod_number;
	r->number = 0;

	return 0;
}

static int
update_add(int type, struct json_value *jv, void *data, MFMOD_PARAM *retval)
{
	MFMOD_PARAM *p = data;
	if (type == value_duration) {
		return mfmod_error(retval, mfe_failure, "%s",
				   "can't increase duration");
	}
	jv->v.n += p->number;
	return 0;
}

/*
 * openmetrics_add(name, qualifier, increment)
 * -------------------------------------------
 * Adds increment to the value of the metric name{qualifier}.
 *
 * Arguments:
 *    p[0]  string     name of the openmetrics
 *    p[1]  string     qualifier
 *    p[2]  number     increment
 *
 * Return: none
 *
 * Exceptions:
 *  mfe_failure    Failed to create temporary database file.
 *  mfe_inval      Bad number or types of arguments.
 *  mfe_dbfailure  GDBM error.
 *  mfe_not_found  Metric not declared (openmetrics_declare should
 *                 have been called first).
 */
int
openmetrics_add(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	datum key;

	ASSERT_ARGCOUNT(r, count, 3);
	ASSERT_ARGTYPE(p, r, 0, mfmod_string);
	ASSERT_ARGTYPE(p, r, 1, mfmod_string);
	ASSERT_ARGTYPE(p, r, 2, mfmod_number);

	key.dptr = p->string;
	key.dsize = strlen(key.dptr);

	return dbmodify(key, p[1].string, update_add, &p[2], 0, r);
}

static int
update_reset(int type, struct json_value *jv, void *data, MFMOD_PARAM *retval)
{
	struct timespec ts;

	switch (type) {
	case value_counter:
	case value_gauge:
		jv->v.n = 0;
		break;

	case value_duration:
		clock_gettime(CLOCK_REALTIME, &ts);
		jv->v.n = (double) ts.tv_sec + (double) ts.tv_nsec / 1e9;
		break;
	}

	return 0;
}

/*
 * openmetrics_reset(name, qualifier)
 * ----------------------------------
 * Resets the metric name{qualifier} to its original value:
 *   - for types counter and gauge - 0.
 *   - for duration - system local time (seconds since Epoch)
 *
 * Arguments:
 *    p[0]  string     name of the metric
 *    p[1]  string     qualifier
 *
 * Return: none
 *
 *  mfe_failure    Failed to create temporary database file.
 *  mfe_inval      Bad number or types of arguments.
 *  mfe_dbfailure  GDBM error.
 *  mfe_not_found  Metric not declared (openmetrics_declare should
 *                 have been called first).
 */
int
openmetrics_reset(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	datum key;

	ASSERT_ARGCOUNT(r, count, 2);
	ASSERT_ARGTYPE(p, r, 0, mfmod_string);
	ASSERT_ARGTYPE(p, r, 1, mfmod_string);

	key.dptr = p->string;
	key.dsize = strlen(key.dptr);

	return dbmodify(key, p[1].string, update_reset, NULL, 0, r);
}

static int
update_set(int type, struct json_value *jv, void *data, MFMOD_PARAM *retval)
{
	MFMOD_PARAM *p = data;
	jv->v.n = p->number;
	return 0;
}

/*
 * openmetrics_set(name, qualifier)
 * --------------------------------
 * Sets the value of the metric name{qualifier}.
 *
 * Arguments:
 *    p[0]  string     metric name
 *    p[1]  string     qualifier
 *    p[2]  number     new value
 *
 * Return: none
 *
 *  mfe_failure    Failed to create temporary database file.
 *  mfe_inval      Bad number or types of arguments.
 *  mfe_dbfailure  GDBM error.
 *  mfe_not_found  Metric not declared (openmetrics_declare should
 *                 have been called first).
 */
int
openmetrics_set(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	datum key;

	ASSERT_ARGCOUNT(r, count, 4);
	ASSERT_ARGTYPE(p, r, 0, mfmod_string);
	ASSERT_ARGTYPE(p, r, 1, mfmod_string);
	ASSERT_ARGTYPE(p, r, 2, mfmod_number);
	ASSERT_ARGTYPE(p, r, 3, mfmod_number);

	key.dptr = p->string;
	key.dsize = strlen(key.dptr);
	return dbmodify(key, p[1].string, update_set, &p[2], p[3].number, r);
}

/*
 * Server
 */
static int
reassemble_get_args(void *cls, enum MHD_ValueKind kind,
		    const char *key, const char *value)
{
	mu_opool_t pool = cls;
	mu_opool_append_char(pool, '&');
	mu_opool_appendz(pool, key);
	if (value) {
		mu_opool_append_char(pool, '=');
		mu_opool_appendz(pool, value);
	}
	return MHD_YES;
}

static int
is_trusted_ip(char const *ipstr)
{
	struct mu_cidr cidr;

	if (mu_cidr_from_string(&cidr, ipstr))
		return 0;
	return mu_list_locate(trusted_ip_list, &cidr, NULL) == 0;
}

static char *
scan_forwarded_header(char const *hdr)
{
	char const *end;
	char *buf = NULL;
	size_t bufsize = 0;

	end = hdr + strlen(hdr);
	while (end > hdr) {
		char const *p;
		size_t len, i, j;

		for (p = end - 1; p > hdr && *p != ','; p--)
			;
		len = end - p;
		if (len + 1 > bufsize) {
			char *newbuf = realloc(buf, len + 1);
			if (newbuf) {
				buf = newbuf;
				bufsize = len + 1;
			} else {
				free(buf);
				return NULL;
			}
		}

		i = 0;
		j = 0;
		if (*p == ',')
			j++;
		while (j < len && isspace(p[j]))
			j++;
		while (j < len) {
			if (isspace(p[j]))
				break;
			buf[i++] = p[j++];
		}
		buf[i] = 0;

		if (!is_trusted_ip(buf))
			return buf;

		end = p;
	}
	free(buf);
	return NULL;
}

char *
get_remote_ip(struct MHD_Connection *conn)
{
	union MHD_ConnectionInfo const *ci;
	char ipstr[NI_MAXHOST];
	char const *hdr;
	char *ret;

	hdr = MHD_lookup_connection_value(conn,
					  MHD_HEADER_KIND,
					  "x-forwarded-for");
	if (hdr && (ret = scan_forwarded_header(hdr)) != NULL)
		return ret;

	ci = MHD_get_connection_info(conn,
				     MHD_CONNECTION_INFO_CLIENT_ADDRESS,
				     NULL);
	if (!ci)
		return NULL;

	if (getnameinfo(ci->client_addr, sizeof(struct sockaddr),
			ipstr, sizeof(ipstr), NULL, 0,
			NI_NUMERICHOST))
		return NULL;
	return strdup(ipstr);
}

static void
http_log(struct MHD_Connection *connection,
	 char const *method, char const *url,
	 int status, size_t size)
{
	char *ipstr;
	char const *host, *referer, *user_agent;
	time_t t;
	struct tm *tm;
	char tbuf[30];
	mu_opool_t pool;
	char *reqs = NULL;

	if (!httpd_access_log)
		return;
	ipstr = get_remote_ip(connection);

	host = MHD_lookup_connection_value(connection,
					   MHD_HEADER_KIND,
					   MHD_HTTP_HEADER_HOST);
	referer = MHD_lookup_connection_value(connection,
					      MHD_HEADER_KIND,
					      MHD_HTTP_HEADER_REFERER);
	user_agent = MHD_lookup_connection_value(connection,
						 MHD_HEADER_KIND,
						 MHD_HTTP_HEADER_USER_AGENT);
	t = time(NULL);
	tm = localtime(&t);
	strftime(tbuf, sizeof(tbuf), "[%d/%b/%Y:%H:%M:%S %z]", tm);

	mu_opool_create(&pool, MU_OPOOL_ENOMEMABRT);
	mu_opool_append_char(pool, '&');
	MHD_get_connection_values(connection, MHD_GET_ARGUMENT_KIND,
				  &reassemble_get_args, pool);
	if (mu_opool_size(pool) > 1) {
		mu_opool_append_char(pool, 0);
		reqs = mu_opool_finish(pool, NULL);
	}

	//FIXME
	if (size > 0)
		mu_error("%s %s - - %s \"%s %s%s\" %3d %zu \"%s\" \"%s\"",
			 host, ipstr, tbuf, method, url, reqs ? reqs : "",
			 status, size,
			 referer ? referer : "-",
			 user_agent ? user_agent : "-");
	else
		mu_error("%s %s - - %s \"%s %s%s\" %3d - \"%s\" \"%s\"",
			 host, ipstr, tbuf, method, url, reqs ? reqs : "",
			 status,
			 referer ? referer : "-",
			 user_agent ? user_agent : "-");

	mu_opool_destroy(&pool);
	free(ipstr);
}

struct openmetrics_iterator
{
	GDBM_FILE dbf;
	datum key;
	METRIC *mtr;
	struct json_pair *pair;

	int state;
	size_t bytes_sent;

	struct MHD_Connection *conn;
	char const *method;
	char const *url;
};

enum {
	STATE_VALUE,
	STATE_TYPE,
	STATE_UNIT,
	STATE_HELP,
	STATE_EOF,
	STATE_FIN
};

static struct openmetrics_iterator *
openmetrics_iterator_init(struct MHD_Connection *conn, char const *method, char const *url)
{
	struct openmetrics_iterator *itr = mu_alloc(sizeof(*itr));
	memset(itr, 0, sizeof(*itr));

	itr->dbf = gdbm_open(dbname, 0, GDBM_READER, 0, NULL);
	if (!itr->dbf) {
		mu_error("can't open database %s for reading: %s (%s)",
			 dbname, gdbm_strerror(gdbm_errno), strerror(errno));
		free(itr);
		return NULL;
	}

	itr->conn = conn;
	itr->method = method;
	itr->url = url;
	itr->state = STATE_VALUE;
	return itr;
}

static ssize_t
response_reader_callback(void *cls, uint64_t pos, char *buf, size_t max)
{
	struct openmetrics_iterator *itr = cls;
	ssize_t total = 0;

# define overflow(n) ((n) < 0 || (n) >= max || !memchr(buf, '\0', (n) + 1))
# define advance(n) (buf += (n), max -= (n), total += (n), itr->bytes_sent += (n))

	if (itr->state == STATE_FIN)
		return MHD_CONTENT_READER_END_OF_STREAM;

	while (1) {
		char const *name;
		int prec;
		double val;
		ssize_t n;
		struct json_value *jv;
		char const *s;

		if (!itr->mtr || itr->pair == NULL) {
			datum key, content;
			MFMOD_PARAM err;
			int rc;

			if (itr->state != STATE_EOF) {
				metric_destroy(&itr->mtr);

				if (itr->key.dptr == NULL)
					key = gdbm_firstkey(itr->dbf);
				else
					key = gdbm_nextkey(itr->dbf, itr->key);
				free(itr->key.dptr);
				itr->key = key;
				if (key.dptr == NULL)
					itr->state = STATE_EOF;
			}

			if (itr->state == STATE_EOF) {
				itr->state = STATE_FIN;
				n = snprintf(buf, max, "%s", "# EOF\n");
				if (overflow(n))
					break;
				advance(n);
				break;
			}

			content = gdbm_fetch(itr->dbf, itr->key);
			if (content.dptr == NULL) {
				mu_error("can't get value for metrics %*.*s: %s",
					 itr->key.dsize, itr->key.dsize, itr->key.dptr,
					 gdbm_strerror(gdbm_errno));
				return MHD_CONTENT_READER_END_WITH_ERROR;
			}

			rc = metric_decode(&content, &key, &itr->mtr, &err);
			free(content.dptr);
			if (rc) {
				mu_error("%s", err.string);
				free(err.string);
				continue;
			}

			if (metric_type_str(itr->mtr->type) == NULL) {
				mu_error("metric %*.*s has unrecognized type: %d",
					 itr->key.dsize, itr->key.dsize, itr->key.dptr,
					 itr->mtr->type);
				/* Skip it */
				continue;
			}

			if (json_object_get(itr->mtr->json, VALUES_ATTR, &jv)
			    || (itr->pair = jv->v.o->head) == NULL)
				continue;

			itr->state = STATE_TYPE;
		}

		switch (itr->state) {
		case STATE_TYPE:
			n = snprintf(buf, max,
				     "# TYPE %s %s\n",
				     itr->mtr->family_name,
				     metric_type_str(itr->mtr->type));

			if (overflow(n))
				goto end;
			advance(n);
			itr->state = STATE_UNIT;
			/* fall through */
		case STATE_UNIT:
			if ((s = metric_get_unit(itr->mtr)) != NULL) {
				n = snprintf(buf, max,
					     "# UNIT %s %s\n",
					     itr->mtr->family_name,
					     s);
				if (overflow(n))
					goto end;
				advance(n);
			}
			itr->state = STATE_HELP;
			/* fall through */
		case STATE_HELP:
			if ((s = metric_get_help(itr->mtr)) != NULL) {
				n = snprintf(buf, max,
					     "# HELP %s %s\n",
					     itr->mtr->family_name,
					     s);
				if (overflow(n))
					goto end;
				advance(n);
			}
			itr->state = STATE_VALUE;
			/* fall through */
		case STATE_VALUE:
			break;
		}

		switch (itr->mtr->type) {
		case value_counter:
		case value_gauge:
			prec = 0;
			val = itr->pair->v->v.n;
			break;

		case value_duration: {
			struct timespec ts;
			clock_gettime(CLOCK_REALTIME, &ts);
			val = (double) ts.tv_sec + (double) ts.tv_nsec / 1e9
				 - itr->pair->v->v.n;
			prec = 3;
			break;
		  }
		}
		name = itr->pair->k;

		n = snprintf(buf, max,
			     "%s%s%s%s %.*f\n",
			     itr->mtr->family_name,
			     name[0] ? "{" : "",
			     name,
			     name[0] ? "}" : "",
			     prec, val);

		if (overflow(n))
			break;
		advance(n);

		itr->pair = itr->pair->next;
	}
end:
	if (total == 0)
		return MHD_CONTENT_READER_END_WITH_ERROR;
	return total;
}

static void
response_free_callback(void *cls)
{
	struct openmetrics_iterator *itr = cls;
	free(itr->key.dptr);
	gdbm_close(itr->dbf);
	free(itr);
}

static int
http_response(struct MHD_Connection *conn,
	      char const *method, char const *url, int status)
{
	int ret;
	struct MHD_Response *resp =
		MHD_create_response_from_buffer(0,
						NULL,
						MHD_RESPMEM_PERSISTENT);
	http_log(conn, method, url, status, 0);
	ret = MHD_queue_response(conn, status, resp);
	MHD_destroy_response(resp);
	return ret;
}

static int
openmetrics_httpd_handler(void *cls,
		      struct MHD_Connection *conn,
		      const char *url, const char *method,
		      const char *version,
		      const char *upload_data, size_t *upload_data_size,
		      void **con_cls)
{
	struct MHD_Response *response;
	int ret;
	struct openmetrics_iterator *itr;

	if (*con_cls == NULL) {
		if (strcmp(method, MHD_HTTP_METHOD_GET))
			return http_response(conn, method, url,
					     MHD_HTTP_METHOD_NOT_ALLOWED);

		if (strcmp(url, "/metrics")) {
			mu_error("bad url: %s", url);
			return http_response(conn, method, url, MHD_HTTP_NOT_FOUND);
		}

		if (!dbname) {
			mu_error("%s", "no database name");
			return http_response(conn, method, url,
					     MHD_HTTP_INTERNAL_SERVER_ERROR);
		}

		if ((itr = openmetrics_iterator_init(conn, method, url)) == NULL)
			return http_response(conn, method, url,
					     MHD_HTTP_INTERNAL_SERVER_ERROR);

		*con_cls = itr;
		return MHD_YES;
	} else
		itr = *con_cls;

	response = MHD_create_response_from_callback(-1, 4096,
						     response_reader_callback,
						     itr,
						     response_free_callback);
	MHD_add_response_header(response,
				MHD_HTTP_HEADER_CONTENT_TYPE,
				"text/plain; version=0.0.4; charset=utf-8");

	ret = MHD_queue_response(conn, MHD_HTTP_OK, response);
	MHD_destroy_response(response);
	return ret;
}

static void
openmetrics_httpd_logger(void *arg, const char *fmt, va_list ap)
{
	mu_verror(fmt, ap);
}

static void
openmetrics_httpd_panic(void *cls, const char *file, unsigned int line,
		    const char *reason)
{
	if (reason)
		mu_error("%s:%d: MHD PANIC: %s", file, line, reason);
	else
		mu_error("%s:%d: MHD PANIC", file, line);
	abort();
}

static int
open_node(char const *node, char const *serv, struct sockaddr **saddr,
	  socklen_t *len)
{
	struct addrinfo hints;
	struct addrinfo *res;
	int reuse;
	int fd;
	int rc;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	rc = getaddrinfo(node, serv, &hints, &res);
	if (rc) {
		mu_error("%s: %s", node, gai_strerror(rc));
		exit(1);
	}

	fd = socket(res->ai_family, res->ai_socktype, 0);
	if (fd == -1) {
		mu_error("socket: %s", strerror(errno));
		return -1;
	}
	reuse = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
		   &reuse, sizeof(reuse));
	if (bind(fd, res->ai_addr, res->ai_addrlen) == -1) {
		mu_error("bind: %s %s",
			 listen_address ? listen_address : DEFAULT_LISTEN_ADDRESS,
			 mu_strerror(errno));
		close(fd);
		return -1;
	}

	if (listen(fd, 8) == -1) {
		mu_error("listen: %s", strerror(errno));
		close(fd);
		return -1;
	}

	if (saddr) {
		*saddr = mu_alloc(res->ai_addrlen);
		*len = res->ai_addrlen;
		if (serv)
			memcpy(*saddr, res->ai_addr, res->ai_addrlen);
		else if (getsockname(fd, (struct sockaddr *) *saddr, len)) {
			mu_error("getsockname: %s", mu_strerror(errno));
			close(fd);
			return -1;
		}
	}
	freeaddrinfo(res);

	return fd;
}

static int
open_listener(char const *addr, struct sockaddr **saddr, socklen_t *salen)
{
	size_t len;
	int fd;

	if (!addr)
		return open_node(DEFAULT_NODE, DEFAULT_SERVICE, saddr, salen);

	if (strcmp(addr, "any") == 0)
		return open_node(DEFAULT_NODE, NULL, saddr, salen);

	len = strcspn(addr, ":");
	if (len == 0)
		fd = open_node(DEFAULT_NODE, addr + 1, saddr, salen);
	else if (addr[len] == 0)
		fd = open_node(addr, DEFAULT_SERVICE, saddr, salen);
	else {
		char *node;
		node = mu_alloc(len + 1);
		memcpy(node, addr, len);
		node[len] = 0;
		fd = open_node(node, addr + len + 1, saddr, salen);
		free(node);
	}
	return fd;
}

static int
server_start(MFMOD_PARAM *retval)
{
	int fd;
	struct sockaddr *sa;
	socklen_t salen;

	if (listen_address && strcmp(listen_address, "none") == 0)
		return 0;

	fd = open_listener(listen_address, &sa, &salen);
	if (fd == -1)
		return mfmod_error(retval, mfe_failure,
				   "can't open HTTP socket");

	if (listen_address == NULL || strcmp(listen_address, "any") == 0) {
		char host[1025];
		char serv[32];
		int rc;

		rc = getnameinfo(sa, salen, host, sizeof(host), serv,
				 sizeof(serv), NI_NUMERICHOST|NI_NUMERICSERV);
		if (rc) {
			mu_error("cannot resolve listen address: %s",
				 gai_strerror(rc));
			close(fd);
			return mfmod_error(retval, mfe_failure,
					   "can't resolve HTTP socket address");
		}
		free(listen_address);
		listen_address = mu_alloc(strlen(host) + strlen(serv) + 2);
		strcpy(listen_address, host);
		strcat(listen_address, ":");
		strcat(listen_address, serv);
	}

	if (!trusted_ip_list) {
		char *val = strdup(DEFAULT_TRUSTED_IP);
		fill_trusted_ip_list(val);
		free(val);
	}

	MHD_set_panic_func(openmetrics_httpd_panic, NULL);
	mhd = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD
			       | MHD_USE_ERROR_LOG,
			       0,
			       NULL, NULL,
			       openmetrics_httpd_handler, NULL,
			       MHD_OPTION_LISTEN_SOCKET, fd,
			       MHD_OPTION_EXTERNAL_LOGGER, openmetrics_httpd_logger, NULL,
			       MHD_OPTION_END);

	return 0;
}
#ifdef TESTCB
int
openmetrics_test_cb(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct openmetrics_iterator *itr;
	char *buffer;
	size_t bufsize;
	uint64_t pos = 0;

	ASSERT_ARGCOUNT(r, count, 1);
	ASSERT_ARGTYPE(p, r, 0, mfmod_number);

	bufsize = p[0].number;
	if (bufsize > 0)
		return -1;

	if ((buffer = malloc(bufsize)) == NULL)
		return -1;

	itr = openmetrics_iterator_init(NULL, "GET", "/metrics");

	for (;;) {
		ssize_t n = response_reader_callback(itr, pos, buffer, bufsize);
		if (n < 0) {
			if (n == MHD_CONTENT_READER_END_OF_STREAM)
				break;
			mu_error("n = %zd\n", n);
			return 1;
		}
		fwrite(buffer, n, 1, stdout);
		pos += n;
	}
	free(buffer);
	return 0;
}
#endif
